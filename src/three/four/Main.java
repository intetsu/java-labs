package three.four;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) throws IOException {
        String basePath = System.getProperty("user.dir") + "/src/three/four/";
        String newPath = basePath + "/" + Long.toString(System.currentTimeMillis());
        createDirectory(newPath);
        String currentDir = basePath + "dir/";
        Files.find(Paths.get(currentDir),
                Integer.MAX_VALUE,
                (filePath, fileAttr) -> fileAttr.isRegularFile())
                .filter(file -> file.getFileName().toString().endsWith(".java"))
                .forEach(file -> {
                    try {
                        String newContent = removeComments(
                                new String(Files.readAllBytes(Paths.get(file.toString())))
                        );
                        String newFilePath = newPath + "/" + file.getFileName();
                        try (BufferedWriter writer = Files.newBufferedWriter(Paths.get(newFilePath))) {
                            writer.write(newContent);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
    }

    private static String removeComments(String input) {
        // removes single-line and multi-line comments

        // first step - to remove single line comments (in case when ' // this  is outer comment for  /* inner not comment \n */' happens)

        String afterFirstStep = Arrays.stream(input.split("\n")).map(line -> {
            int startingIndex = line.indexOf("//");
            if(startingIndex == -1)
                return line;
            else
                return line.substring(0, startingIndex);
        }).collect(Collectors.joining("\n"));

        // second step - remove multiline comments
        String result = afterFirstStep.replaceAll("(?s)/\\*.*?\\*/","");

        return result;
    }

    private static void createDirectory(String dirName) {
        File theDir = new File(dirName);
        if (!theDir.exists()) {
            System.out.println("creating directory: " + theDir.getName());
            boolean result = false;

            try{
                theDir.mkdir();
                result = true;
            }
            catch(SecurityException se){
            }
            if(result) {
                System.out.println("DIR created");
            }
        }
    }
}
