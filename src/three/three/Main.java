package three.three;

import java.util.Collections;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class Main {
    public final static int THREAD_POOL_SIZE = 5;

    public static Map<String, Integer> hashTable = null;
    public static Map<String, Integer> synchronizedMap = null;
    public static Map<String, Integer> concurrentHashMap = null;

    public static void main(String[] args) {

        ExecutorService executorService = Executors.newFixedThreadPool(THREAD_POOL_SIZE);

        hashTable = new Hashtable<>();
        synchronizedMap = Collections.synchronizedMap(new HashMap<String, Integer>());
        concurrentHashMap = new ConcurrentHashMap<>();

        executeTest(executorService, hashTable);
        executeTest(executorService, synchronizedMap);
        executeTest(executorService, concurrentHashMap);

        executorService.shutdown();
    }

    private static void executeTest(ExecutorService s, Map m) {
        s.execute(() -> {
            try {
                performTest(m);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
    }

    public static void performTest(final Map<String, Integer> map) throws InterruptedException {

        System.out.println("Test started for: " + map.getClass());
        long averageTime = 0;
        for (int i = 0; i < 5; i++) {

            long startTime = System.nanoTime();
            ExecutorService executorService = Executors.newFixedThreadPool(THREAD_POOL_SIZE);

            for (int j = 0; j < THREAD_POOL_SIZE; j++) {
                executorService.execute(() -> {

                    for (int i1 = 0; i1 < 100000; i1++) {
                        Integer randomNumber = (int) Math.ceil(Math.random() * 550000);

                        // Retrieve value. We are not using it anywhere
                        Integer retrievedValue = map.get(String.valueOf(randomNumber));

                        // Put value
                        map.put(String.valueOf(randomNumber), randomNumber);
                    }
                });
            }

            // Make sure executor stops
            executorService.shutdown();

            // Blocks until all tasks have completed execution after a shutdown request
            executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.DAYS);

            long entTime = System.nanoTime();
            long totalTime = (entTime - startTime) / 1000000L;
            averageTime += totalTime;
        }
        System.out.println("For " + map.getClass() + " the average time is " + averageTime / 5 + " ms\n");
    }

}
