package three.two;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicLong;

public class ThreadStuff {

    public AtomicLong someVariable = new AtomicLong();

    public void smth() {
        Runnable counter = () -> { synchronized ( someVariable) {while(someVariable.incrementAndGet() < 1000000) ;
            try {
                someVariable.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }};
        Runnable printer = () -> {synchronized (someVariable) {while(someVariable.get() < 1000000) System.out.print(someVariable + " "); someVariable.notify();}};

        Thread counterThread = new Thread(counter);
        Thread printerThread = new Thread(printer);

        counterThread.start();
        printerThread.start();
    }
}
