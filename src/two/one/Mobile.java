package two.one;

import java.util.ArrayList;
import java.util.List;

import static java.util.stream.Collectors.joining;

public class Mobile {
    private String mobileModel;
    private List<Component> mobileComponents = new ArrayList<>();

    public Mobile(String model) {
        this.mobileModel = model;
        this.mobileComponents.add(new Component("Basic component 1", 1));
        this.mobileComponents.add(new Component("Basic component 2", 2));
    }

    public void addComponent(String name, int version) {
        this.mobileComponents.add(new Component(name, version));
    }

    class Component {
        private String name;
        private int version;

        public Component(String name, int version) {
            this.name = name;
            this.version = version;
        }

        @Override
        public String toString() {
            return name + " of version " + version;
        }
    }

    @Override
    public String toString() {
        String components = mobileComponents.stream().map(c -> c.toString()).collect(joining("\n"));
        return String.format("%s\n%s", mobileModel, components);
    }
}
