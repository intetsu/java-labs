package two.one;

public class Main {
    public static void main(String[] args) {
        Mobile nokia = new Mobile("Nokia 9300");
        Mobile iphone = new Mobile("Iphone");

        nokia.addComponent("big screen", 155);
        nokia.addComponent("significant camera", 12);

        iphone.addComponent("nothing", 0);

        System.out.println(nokia);
        System.out.println(iphone);
    }
}
