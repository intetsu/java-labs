package two.two;

public class Main {
    public static void main(String[] args) {
        Boing boing = new Boing(100, 100, 100, 0);
        NotBoing notBoing = new NotBoing(0, 0, 0, 2);

        Aircompany air = new Aircompany();
        air.addPlane(boing);
        air.addPlane(notBoing);

        assert air.filterByConsuming(1).contains(notBoing);
        assert air.filterByMistkist(90).contains(boing);
    }
}
