package two.two;

public abstract class Plane {
    private int flightLength;
    private int mistkist;
    private int weightOfCanFlyWith;
    private int consumingPalne;

    public Plane(int l, int m, int w, int c) {
        this.flightLength = l;
        this.mistkist = m;
        this.weightOfCanFlyWith = w;
        this.consumingPalne = c;
    }
    public int getWeightOfCanFlyWith() {
        return weightOfCanFlyWith;
    }

    public int getConsumingPalne() {
        return consumingPalne;
    }

    public int getFlightLength() {
        return flightLength;
    }

    public int getMistkist() {
        return mistkist;
    }
}