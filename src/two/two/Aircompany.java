package two.two;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Aircompany {
    public List<Plane> planes = new ArrayList<>();

    public void addPlane(Plane plane) {
        planes.add(plane);
    }

    public List<Plane> filterByMistkist(int m) {
        return planes.stream().filter(p -> p.getMistkist() >= m).collect(Collectors.toList());
    }

    public List<Plane> filterByConsuming(int c) {
        return planes.stream().filter(p -> p.getConsumingPalne() >= c).collect(Collectors.toList());
    }
}
