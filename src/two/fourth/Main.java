package two.fourth;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Main {
    public static void main(String[] args) throws IOException {

        String path = Main.class.getResource("text.txt").getPath();
        String content = new String(Files.readAllBytes(Paths.get(path)));
        String[] words = content.split("(?<=\\W)|(?=\\W)");
        StringBuilder sb = new StringBuilder();
        for (String word : words) {
            if (word.length() > 0)
                sb.append(word.substring(0, 1).toUpperCase()).append(word.substring(1).toLowerCase());
        }
        String newContent = sb.toString();
        Files.write(Paths.get(path), newContent.getBytes());
    }
}
