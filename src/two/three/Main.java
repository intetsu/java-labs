package two.three;

import java.util.Stack;

public class Main {
    public static void main(String[] args) {
        Stack<Integer> myStack = new Stack<>();
        int rand = (int)(Math.random() *899999);
        int randSnapshot = rand;
        while(rand > 0) {
            int c = rand % 10;
            rand /= 10;
            myStack.push(c);
        }
        int result = 0;
        for(int i = 0; i < 6; i++){
            result += (myStack.pop()*Math.pow(10, i));
        }
        System.out.println(String.format("Original: %d . New: %d", randSnapshot, result));
    }
}
