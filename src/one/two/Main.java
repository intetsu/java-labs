package one.two;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String text = "o tw thr four fivee sixxxx sevennn eightttt";

        System.out.println("Please, input restricted length:");
        System.out.println(
                deleteAllWordsOfLength(sc.nextInt(), text)
        );

    }

    public static String deleteAllWordsOfLength(int restrictedLength, String text) {
        return text.replaceAll(String.format("\\b[\\w']{%s}\\b", restrictedLength), "");
    }
}
