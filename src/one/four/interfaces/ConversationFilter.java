package one.four.interfaces;

import one.four.entities.Conversation;

@FunctionalInterface
public interface ConversationFilter {
    boolean filter(Conversation conversation);
}