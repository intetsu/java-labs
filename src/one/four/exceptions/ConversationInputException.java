package one.four.exceptions;

public class ConversationInputException extends Exception{

    public ConversationInputException(String message)
    {
        super(message);
    }
}
