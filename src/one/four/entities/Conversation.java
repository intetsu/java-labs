package one.four.entities;

import one.four.enums.ConversationType;
import one.four.exceptions.ConversationInputException;
import org.apache.log4j.Logger;

import java.time.LocalDateTime;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Conversation {
    private final Logger logger = Logger.getLogger(Abonent.class);

    private LocalDateTime start;
    private LocalDateTime finish;
    private ConversationType type;

    public Conversation() throws ConversationInputException{
        LocalDateTime start, finish;
        ConversationType resultType;
        Scanner sc = new Scanner(System.in);
        System.out.println("Please, enter start date in format YYYY-MM-DDTHH:MM:SS (as in example 2007-12-03T10:15:30):");
        try {
            start = LocalDateTime.parse(sc.nextLine());
            logger.info("Start date was successfully read.");
        } catch (Exception e) {
            logger.fatal("Start date reading turned into fail.");
            throw new ConversationInputException("You've typed start date in wrong format");
        }
        System.out.println("Please, enter date in format YYYY-MM-DDTHH:MM:SS (as in example 2007-12-03T10:15:30):");
        try {
            finish = LocalDateTime.parse(sc.nextLine());
            logger.info("Finish date was successfully read.");
        } catch (Exception e) {
            logger.fatal("Finish date reading turned into fail.");
            throw new ConversationInputException("You've typed start date in wrong format");
        }
        System.out.println("Please, print 0 if it was incity or 1 if it was outcity:");
        try {
            int type = sc.nextInt();
            if(type == 0) resultType = ConversationType.INCITY;
            else if (type == 1) resultType = ConversationType.OUTCITY;
            else throw new InputMismatchException();
        } catch (Exception ex) {
            logger.fatal("ConversationType reading turned into fail.");
            throw new ConversationInputException("You've typed wrong conversation type number.");
        }
        this.start = start;
        this.finish = finish;
        this.type = resultType;
    }

    public LocalDateTime getStart() {
        return start;
    }

    public LocalDateTime getFinish() {
        return finish;
    }

    public ConversationType getType() {
        return type;
    }
}



