package one.four.entities;

import one.four.interfaces.ConversationFilter;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

public class Abonent {
    private final Logger logger = Logger.getLogger(Abonent.class);

    private String firstName;
    private String lastName;
    private String patronymic;

    private Address address;

    private List<Conversation> conversations;

    public Abonent(String firstName, String lastName, String patronymic) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.patronymic = patronymic;
        conversations = new ArrayList<>();
        logger.info(String.format("Abonent %s was created.", this));
    }


    public void addConversation(Conversation conversation){

        this.conversations.add(conversation);
        logger.info(String.format("Conversation %s was added!", conversation));
    }

    public boolean conversationsExist(ConversationFilter filter){
        return conversations.stream().anyMatch(filter::filter);
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    @Override
    public String toString() {
        logger.info("Someone stringified abonent!");
        if(address == null)
            return String.format("%s %s %s", lastName, firstName, patronymic);
        else
            return String.format("%s %s %s\t %s", lastName, firstName, patronymic, address);
    }
}
