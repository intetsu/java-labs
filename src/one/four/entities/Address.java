package one.four.entities;

public class Address {
    private String city;
    private String street;
    private int streetNumber;
    private int appartmentNumber;

    public Address(String city, String street, int streetNumber, int appartmentNumber){
        this.city = city;
        this.street = street;
        this.streetNumber = streetNumber;
        this.appartmentNumber = appartmentNumber;
    }

    @Override
    public String toString() {
        return String.format("%s %s %s %s", city, street, streetNumber, appartmentNumber);
    }
}
