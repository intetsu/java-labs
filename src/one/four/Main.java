package one.four;

import one.four.entities.Abonent;
import one.four.entities.Conversation;
import one.four.enums.ConversationType;
import one.four.exceptions.ConversationInputException;

import java.time.Duration;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {
        Abonent first = new Abonent("Yaroslav", "Krukovsky", "Olexandrovich");
        Abonent second = new Abonent("Anton", "Vasiliev", "Olexandrovich");
        Abonent third = new Abonent("Petya", "Savchuk", "Evgenievich");
        Abonent fourth = new Abonent("Petro", "Salnikov", "Ivanovich");

        List<Abonent> abonents = Arrays.asList(first, second, third, fourth);
        // conversation input block
        abonents.forEach(a -> {
            try {
                a.addConversation(new Conversation());
            } catch (ConversationInputException e) {
                System.out.println("Abonent " + a.toString() + " will not receive his conversation data because: " + e.getMessage());
            }
        });

        System.out.println("More than needed time speakers: ");
        printAbonentList(moreThanNeededSpeakers(300, abonents));
        System.out.println("\n\nOutcity speakers: ");
        printAbonentList(conversatedOutcity(abonents));
}

    public static List<Abonent> moreThanNeededSpeakers(int neededSeconds, List<Abonent> abonents){
        return abonents.stream().filter(a -> a.conversationsExist(c -> {
            Duration dur = Duration.between(c.getStart(), c.getFinish());
            return neededSeconds < (dur.toMillis()/1000);
        })).collect(Collectors.toList());
    }

    public static List<Abonent> conversatedOutcity(List<Abonent> abonents){
        return abonents.stream().filter(a -> a.conversationsExist(c -> c.getType() == ConversationType.OUTCITY)).collect(Collectors.toList());
    }

    public static void printAbonentList(List<Abonent> abonents) {
        System.out.println("FULL NAME:\t ADDRESS:");
        abonents.forEach(a -> System.out.println(a.toString()));
    }


}
