package one.one;

import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Please, input N: ");
        int N = sc.nextInt();
        float[][] originalArr = new float[N][N];
        float[][] newArr = new float[N][N];
        for(int i = 0; i < N; i++)
            for(int j = 0; j < N; j++){
                originalArr[i][j] = (float)Math.random();
            }
        newArr[0] = Arrays.copyOf(originalArr[originalArr.length-1], originalArr[originalArr.length-1].length);
        for(int i=0; i<originalArr.length-1; ++i) {
            newArr[i+1] = Arrays.copyOf(originalArr[i], originalArr[i].length);
        }

        System.out.println("Developer: Yaroslav Krukovsky");
        System.out.println("Original array:");
        for (float[] x : originalArr) {
            System.out.println(Arrays.toString(x));
        }
        System.out.println("New array:");
        for (float[] x : newArr) {
            System.out.println(Arrays.toString(x));
        }
    }
}
