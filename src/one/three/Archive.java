package one.three;

import java.util.HashMap;

public class Archive {
    private HashMap<Course, Tuple<Pupil, Integer>> courses = new HashMap<>();

    public void update(Course course, Pupil pupil, int mark) {
        if(!course.containPupil(pupil)) {
            System.out.println(String.format("Pupil %s was not added to course %s", pupil, course));
        } else{
            this.courses.put(course, new Tuple<Pupil, Integer>(pupil, mark));
            System.out.println(String.format("Add pupil %s's with mark %d of course %s", pupil, mark, course));
        }

    }
}
