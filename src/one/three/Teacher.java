package one.three;

public class Teacher extends HumanBeing{
    public Teacher(String fullName) {
        super(fullName);
    }

    public void setToCourse(Course course, Pupil pupil) {
        System.out.println(
                String.format("%s adding pupil %s to course %s", this, pupil, course)
        );
        course.addPupil(pupil);
    }
}
