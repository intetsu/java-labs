package one.three;

public class Main {
    public static void main(String[] args) {
        Teacher t = new Teacher("Vasiliy");
        Pupil p1 = new Pupil("firstPupil");
        Pupil p2 = new Pupil("secondPupil");
        Pupil p3 = new Pupil("thirdPupil");
        Pupil p4 = new Pupil("fourthPupil");

        Course first = new Course("firstCourse");
        Course second = new Course("secondCourse");

        Archive archive = new Archive();
        t.setToCourse(first, p1);
        t.setToCourse(first, p2);
        t.setToCourse(first, p3);
        t.setToCourse(second, p2);
        t.setToCourse(second, p3);
        t.setToCourse(second, p4);

        System.out.println("___________________________________");
        archive.update(first, p1, 12);
        archive.update(first, p2, 10);
        archive.update(first, p4, 8);
        archive.update(second, p1, 12);
        archive.update(second, p3, 2);
    }
}

