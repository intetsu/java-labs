package one.three;

import java.util.ArrayList;
import java.util.List;

public class Course {
    private String courseName;
    private List<Pupil> pupils = new ArrayList<>();

    public Course(String name){
        this.courseName = name;
    }

    public void addPupil(Pupil pupil) {
        if(!this.containPupil(pupil)) {
            System.out.println(String.format("Pupil %s was added to course %s", pupil, courseName));
            pupils.add(pupil);
        } else {
            System.out.println(String.format("Pupil %s already is in course %s", pupil, courseName));
        }
    }

    public boolean containPupil(Pupil pupil) {
        return pupils.contains(pupil);
    }

    @Override
    public String toString() {
        return courseName;
    }
}
