package one.three;

public class HumanBeing {
    private String fullName;

    public HumanBeing(String fullName){
        this.fullName = fullName;
    }

    @Override
    public String toString() {
        return fullName;
    }
}
